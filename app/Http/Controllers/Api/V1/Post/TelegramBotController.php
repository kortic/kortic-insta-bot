<?php

namespace App\Http\Controllers\Api\V1\Post;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Post\Post;
use InvalidArgumentException;

class TelegramBotController extends Controller
{
	public function webhook(Request $request)
	{
		\Log::info($request);

        $botKey = '777763290:AAHioLy9iGz6S-ezmZcUi-rCcpiO4n_-Pkg';

        $requestFactory = new \Http\Factory\Guzzle\RequestFactory();
        $streamFactory = new \Http\Factory\Guzzle\StreamFactory();
        $client = new \Http\Adapter\Guzzle6\Client();

        $apiClient = new \TgBotApi\BotApiBase\ApiClient($requestFactory, $streamFactory, $client);
        $bot = new \TgBotApi\BotApiBase\BotApiComplete($botKey, $apiClient, new \TgBotApi\BotApiBase\BotApiNormalizer());

        $userId = $request['message']['chat']['id'];

		try {
	        $instagram = new \InstagramScraper\Instagram();
	        $media = $instagram->getMediaByUrl($request['message']['text']);

			\Log::info($media);

            Post::create([
	            'link' => $request['message']['text'],
	            'type' => 'insta',
	            'post_id' => $media->getImageLowResolutionUrl(),
	        ]);

	        $bot->sendMessage(\TgBotApi\BotApiBase\Method\SendMessageMethod::create($userId, 'все круто, крошка ❤️'));

		} catch (InvalidArgumentException $e) {
			$message = $e->getMessage();

	        $bot->sendMessage(\TgBotApi\BotApiBase\Method\SendMessageMethod::create($userId, 'что-то пошло не так, крошка ❤️ это точно ссылка на инсту?'));
		}
	}
}
