<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
	'prefix' => 'v1/posts',
	'as' => 'posts.',
	'namespace' => 'Api\V1\Post',
], function () {
    Route::get('', 'PostController@index');
    Route::post('', 'PostController@store');
    Route::post('bot-webhook', 'TelegramBotController@webhook');
});
